package test.com.marchenko.analyzer.action;

import com.marchenko.analyzer.action.TextAction;
import com.marchenko.analyzer.entity.CompositeText;
import com.marchenko.analyzer.entity.Text;
import com.marchenko.analyzer.parser.TextParserThread;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * Created by Marchenko Vadim on 10/14/2015.
 */
public class TextActionTest {
    private static final Logger LOGGER = LogManager.getLogger(TextAction.class);

    @Test
    public void swapFirstLastWordsSentencesTest(){
        String textString = "Hello a beautiful bird!";
        TextAction action = new TextAction();
        Text text = new CompositeText(CompositeText.Type.SENTENCE);

        int oneThisThreadAndOneSentence = 2;
        CyclicBarrier barrier = new CyclicBarrier(oneThisThreadAndOneSentence);
        TextParserThread parser = new TextParserThread(text, textString, TextParserThread.TypeParsingText.SENTENCE, barrier);
        parser.start();

        try {
            barrier.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            LOGGER.warn("Current thread was interrupted", e);
        }

        action.swapFirstLastWordsSentences((CompositeText)text);

        String expected = "bird a beautiful Hello! ";
        Assert.assertEquals(expected, text.toString());
    }
}
