package test.com.marchenko.analyzer.parser;

import com.marchenko.analyzer.entity.CompositeText;
import com.marchenko.analyzer.entity.PrimeText;
import com.marchenko.analyzer.entity.Text;
import com.marchenko.analyzer.parser.TextParserThread;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * Created by Marchenko Vadim on 10/14/2015.
 */
public class TextParserThreadTest {
    private static final Logger LOGGER = LogManager.getLogger(TextParserThreadTest.class);

    @Test
    public void runTest() throws InterruptedException {
        String toParse = "Hello world System.out.println()!";

        Text text = new CompositeText(CompositeText.Type.SENTENCE);

        int oneThisThreadAndOneSentence = 2;
        CyclicBarrier barrier = new CyclicBarrier(oneThisThreadAndOneSentence);
        TextParserThread parser = new TextParserThread(text, toParse, TextParserThread.TypeParsingText.SENTENCE, barrier);
        parser.start();

        try {
            barrier.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            LOGGER.warn("Current thread was interrupted", e);
        }

        ArrayList<String> listWords = new ArrayList<>();
        getWords(listWords, text);

        String[] expectedWords = new String[]{
                "Hello", "world", "System", "out", "println"
        };
        int expectedSizeWord = 5;

        Assert.assertEquals(listWords.size(), expectedSizeWord, 0);
        for (int i = 0; i < expectedSizeWord; ++ i){
            Assert.assertEquals(listWords.get(i), expectedWords[i]);
        }


        ArrayList<String> listLexeme = new ArrayList<>();
        getLexeme(listLexeme, text);

        String[] expectedLexeme = new String[]{
                "Hello", "world", "System.out.println()"
        };
        int expectedSizeLexeme = 3;

        Assert.assertEquals(listLexeme.size(), expectedSizeLexeme, 0);
        for (int i = 0; i < expectedSizeLexeme; ++ i){
            Assert.assertEquals(listLexeme.get(i), expectedLexeme[i]);
        }
    }

    private void getWords(ArrayList<String> words, Text text){
        if ( (text instanceof PrimeText) &&
                ((PrimeText) text).getType() == PrimeText.Type.WORD){
            words.add(((PrimeText) text).getContent());
        }

        for (int i = 0; i < text.size(); ++ i){
            getWords(words, text.getChild(i));
        }
    }

    private void getLexeme(ArrayList<String> words, Text text){
        if ( (text instanceof CompositeText) &&
                ((CompositeText) text).getType() == CompositeText.Type.LEXEME){
            words.add(text.toString());
        }

        for (int i = 0; i < text.size(); ++ i){
            getLexeme(words, text.getChild(i));
        }
    }
}
