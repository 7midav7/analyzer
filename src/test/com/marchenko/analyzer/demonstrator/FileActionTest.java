package test.com.marchenko.analyzer.demonstrator;

import com.marchenko.analyzer.demonstrator.FileAction;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Marchenko Vadim on 10/14/2015.
 */
public class FileActionTest {

    @Test(expected = IOException.class)
    public void readFileTest() throws IOException{
        FileAction fileAction = new FileAction();
        fileAction.readFile("nonexistentFile");
    }
}
