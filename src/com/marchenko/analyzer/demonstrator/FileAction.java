package com.marchenko.analyzer.demonstrator;


import java.io.*;

/**
 * Created by Marchenko Vadim on 10/14/2015.
 */
public class FileAction {

    public String readFile(String name) throws IOException{
        StringBuilder builder = new StringBuilder();

        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(name))) {
            String temp = bufferedReader.readLine();
            while (temp != null) {
                builder.append(temp).append("\n");
                temp = bufferedReader.readLine();
            }
        }

        return builder.toString();
    }

    public void writeFile(String name, String content) throws IOException{
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(name))){
            bufferedWriter.write(content);
        }
    }
}
