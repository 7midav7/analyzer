package com.marchenko.analyzer.demonstrator;

import com.marchenko.analyzer.action.TextAction;
import com.marchenko.analyzer.entity.CompositeText;
import com.marchenko.analyzer.parser.TextParserThread;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * Created by Marchenko Vadim on 10/6/2015.
 */
public class Demonstrator {
    private static final Logger LOGGER = LogManager.getLogger(Demonstrator.class);
    private static final String NAME_INPUT = "inputpars\\book.txt";
    private static final String NAME_OUTPUT_PARSE = "outputpars\\parse.txt";
    private static final String NAME_OUTPUT_SORTING = "outputpars\\sorting.txt";
    private static final String NAME_OUTPUT_SWAPPING = "outputpars\\swapping.txt";

    public void demonstrate() throws IOException{
        FileAction fileAction = new FileAction();
        String text = fileAction.readFile(NAME_INPUT);

        CompositeText composite = new CompositeText(CompositeText.Type.CHAPTER);

        int oneThisThreadAndOneSentence = 2;
        CyclicBarrier barrier = new CyclicBarrier(oneThisThreadAndOneSentence);
        TextParserThread parser = new TextParserThread(composite, text, TextParserThread.TypeParsingText.CHAPTER, barrier);
        parser.start();

        try {
            barrier.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            LOGGER.warn("Current thread was interrupted", e);
        }

        fileAction.writeFile(NAME_OUTPUT_PARSE, composite.toString());

        TextAction textAction = new TextAction();

        fileAction.writeFile(NAME_OUTPUT_SORTING ,textAction.orderedSentencesSize(composite));

        textAction.swapFirstLastWordsSentences(composite);
        fileAction.writeFile(NAME_OUTPUT_SWAPPING, composite.toString());
    }
}
