package com.marchenko.analyzer.main;

import com.marchenko.analyzer.demonstrator.Demonstrator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Created by Marchenko Vadim on 10/6/2015.
 */
public class Main {
    private static final Logger LOGGER;
    private static final String LOGS_CONFIG_URI = "config\\log4j2.xml";

    static{
        System.setProperty("log4j.configurationFile", LOGS_CONFIG_URI);
        LOGGER = LogManager.getLogger(Main.class);
    }

    public static void main(String[] args) {
        Demonstrator demonstrator = new Demonstrator();
        try {
            demonstrator.demonstrate();
        } catch (IOException e){
            LOGGER.warn("Problems with files", e);
        }
    }
}
