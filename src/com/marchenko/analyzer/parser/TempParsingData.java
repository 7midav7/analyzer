package com.marchenko.analyzer.parser;

import com.marchenko.analyzer.entity.Text;

/**
 * Created by Marchenko Vadim on 10/13/2015.
 */
class TempParsingData {
    private String stringParse;
    private Text textChild;
    private TextParserThread.TypeParsingText type;

    public TempParsingData(String stringParse, Text textChild, TextParserThread.TypeParsingText type) {
        this.stringParse = stringParse;
        this.textChild = textChild;
        this.type = type;
    }

    public String getStringParse() {
        return stringParse;
    }

    public Text getTextChild() {
        return textChild;
    }

    public TextParserThread.TypeParsingText getTypeParsingText() {
        return type;
    }
}
