package com.marchenko.analyzer.parser;

import com.marchenko.analyzer.entity.CompositeText;
import com.marchenko.analyzer.entity.PrimeText;
import com.marchenko.analyzer.entity.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Marchenko Vadim on 10/6/2015.
 */
public class TextParserThread extends Thread{
    public enum TypeParsingText {
        CHAPTER("(.+(\n)?)+"),
        PARAGRAPH("[^ ](.|(\n))*?([\\.?!][ \n\"])"),
        SENTENCE("\\p{Alnum}+((\\.\\p{Alnum}+)?+(\\(.??\\))?)*"),
        LEXEME("\\p{Alnum}+"),
        NOTHING_PARSE("");

        private String regexParsing;

        TypeParsingText(String regexParsing) {
            this.regexParsing = regexParsing;
        }

        public String getRegexParsing() {
            return regexParsing;
        }
    }

    private static final Logger LOGGER = LogManager.getLogger(TextParserThread.class);

    private TypeParsingText type;
    private Text mutableText;
    private String stringParse;
    private CyclicBarrier barrierParent;

    public TextParserThread(Text mutableText, String stringParse, TypeParsingText type, CyclicBarrier barrierParent) {
        this.mutableText = mutableText;
        this.type = type;
        this.stringParse = stringParse;
        this.barrierParent = barrierParent;
    }

    @Override
    public void run() {
        ArrayList<TempParsingData> list = findComponentsChildren(stringParse);

        CyclicBarrier barrier = new CyclicBarrier(list.size() + 1);

        for (TempParsingData tpd : list){
            mutableText.addChild(tpd.getTextChild());
            TextParserThread parser  = new TextParserThread(tpd.getTextChild(), tpd.getStringParse(),
                    tpd.getTypeParsingText(), barrier);
            parser.start();
        }

        try {
            barrier.await();
            barrierParent.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            LOGGER.warn("Barrier crushed", e);
        }
    }

    private ArrayList<TempParsingData> findComponentsChildren(String stringParse){
        ArrayList<TempParsingData> list = new ArrayList<>();

        if (type == TypeParsingText.NOTHING_PARSE){
            return list;
        }

        Pattern pattern = Pattern.compile(type.getRegexParsing());
        Matcher matcher = pattern.matcher(stringParse);

        int indexAnotherSymbol = 0;
        while (matcher.find()){
            String mainSymbols = matcher.group();
            String anotherSymbols = stringParse.substring(indexAnotherSymbol, matcher.start());
            addAnotherSymbols(list, anotherSymbols);
            addMainSymbols(list, mainSymbols);
            indexAnotherSymbol = matcher.end();
        }
        if (indexAnotherSymbol < stringParse.length()) {
            String anotherSymbols = stringParse.substring(indexAnotherSymbol);
            addAnotherSymbols(list, anotherSymbols);
        }

        return list;
    }

    private void addMainSymbols(ArrayList<TempParsingData> mutableList, String stringParse){
        switch (type){
            case NOTHING_PARSE:
                break;
            case LEXEME:
                mutableList.add(new TempParsingData(
                                stringParse, new PrimeText(PrimeText.Type.WORD, stringParse), TypeParsingText.NOTHING_PARSE)
                );
                break;
            case SENTENCE:
                mutableList.add(new TempParsingData(
                                stringParse, new CompositeText(CompositeText.Type.LEXEME), TypeParsingText.LEXEME)
                );
                break;
            case PARAGRAPH:
                mutableList.add(new TempParsingData(
                                stringParse, new CompositeText(CompositeText.Type.SENTENCE), TypeParsingText.SENTENCE)
                );
                break;
            case CHAPTER:
                if ("//:".equals(stringParse.substring(0,3))) {
                    mutableList.add(new TempParsingData(
                                    stringParse, new PrimeText(PrimeText.Type.LISTING, stringParse), TypeParsingText.NOTHING_PARSE)
                        );
                } else {
                    mutableList.add(new TempParsingData(
                                    stringParse, new CompositeText(CompositeText.Type.PARAGRAPH), TypeParsingText.PARAGRAPH)
                    );
                }
            default:
                LOGGER.warn("Unknown type of " + TypeParsingText.class.getName());
        }
    }

    private void addAnotherSymbols(ArrayList<TempParsingData> mutableList, String string){
        if (!string.isEmpty()) {
            mutableList.add(new TempParsingData(string,
                    new PrimeText(PrimeText.Type.ANOTHER_SYMBOLS, string),
                    TypeParsingText.NOTHING_PARSE));
        }
    }

}
