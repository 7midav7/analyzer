package com.marchenko.analyzer.action;

import com.marchenko.analyzer.entity.Text;
import com.marchenko.analyzer.entity.CompositeText;
import com.marchenko.analyzer.entity.PrimeText;

import java.util.ArrayList;

/**
 * Created by Marchenko Vadim on 10/14/2015.
 */
public class TextAction {

    public String orderedSentencesSize(CompositeText compositeText){
        StringBuilder builder = new StringBuilder();
        ArrayList<CompositeText> list = new ArrayList<>();
        findSentence(list, compositeText);

        list.sort((o1, o2) -> numberWordsComposite(o1) - numberWordsComposite(o2));

        for (CompositeText text: list){
            String string = text.toString();
            String withSpace = string.replaceAll("\n", " ");
            builder.append(withSpace).append("\n");
        }

        return builder.toString();
    }

    public void swapFirstLastWordsSentences(CompositeText compositeText){
        ArrayList<CompositeText> list = new ArrayList<>();
        findSentence(list, compositeText);
        for (CompositeText text: list){
            PrimeText firstWord = findFirstWord(text);
            PrimeText lastWord = findLastWord(text);

            if (firstWord == null || lastWord == null){
                continue;
            }

            String swapContent = firstWord.getContent();
            firstWord.setContent(lastWord.getContent());
            lastWord.setContent(swapContent);
        }
    }

    private void findSentence(ArrayList<CompositeText> mutableList, CompositeText compositeText){

        if (compositeText.getType() == CompositeText.Type.SENTENCE){
            mutableList.add(compositeText);
            return;
        }

        for (int i = 0; i < compositeText.size(); ++ i){
            Text text = compositeText.getChild(i);
            if (text instanceof CompositeText) {
                findSentence(mutableList, (CompositeText) text);
            }
        }
    }

    private int numberWordsComposite(Text text){
        if ((text instanceof PrimeText) &&
                ((PrimeText) text).getType()== PrimeText.Type.WORD){
            return 1;
        }

        int sum = 0;
        for (int i = 0; i < text.size(); ++ i){
            Text c = text.getChild(i);
            sum += numberWordsComposite(c);
        }

        return sum;
    }

    private PrimeText findFirstWord(Text text){
        if ((text instanceof PrimeText) &&
                ((PrimeText) text).getType()== PrimeText.Type.WORD){
            return (PrimeText) text;
        }

        PrimeText result = null;

        int index = 0;
        while (index < text.size()){
            if (findFirstWord(text.getChild(index)) != null){
                result = findFirstWord(text.getChild(index));
                break;
            }
            ++ index;
        }

        return result;
    }

    private PrimeText findLastWord(Text text){
        if ((text instanceof PrimeText) &&
                ((PrimeText) text).getType()== PrimeText.Type.WORD){
            return (PrimeText) text;
        }

        PrimeText result = null;

        int index = text.size()-1;
        while (index >= 0){
            if (findFirstWord(text.getChild(index)) != null){
                result = findLastWord(text.getChild(index));
                break;
            }
            -- index;
        }

        return result;
    }
}
