package com.marchenko.analyzer.entity;

/**
 * Created by Marchenko Vadim on 10/6/2015.
 */
public interface Text {
    boolean addChild(Text let);
    Text getChild(int id);
    int size();
}
