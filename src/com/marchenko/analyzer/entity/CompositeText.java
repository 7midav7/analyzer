package com.marchenko.analyzer.entity;


import java.util.ArrayList;

/**
 * Created by Marchenko Vadim on 10/6/2015.
 */
public class CompositeText implements Text {
    public enum Type{CHAPTER, PARAGRAPH, SENTENCE, LEXEME}

    private Type type;
    private ArrayList<Text> texts = new ArrayList<>();

    public CompositeText(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    @Override
    public boolean addChild(Text comp) {
        texts.add(comp);
        return true;
    }

    @Override
    public Text getChild(int id) {
        return texts.get(id);
    }

    @Override
    public int size() {
        return texts.size();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Text text : texts){
            builder.append(text.toString());
        }
        return builder.toString();
    }
}
