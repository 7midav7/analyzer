package com.marchenko.analyzer.entity;


/**
 * Created by Marchenko Vadim on 10/6/2015.
 */
public class PrimeText implements Text {
    public enum Type{WORD, ANOTHER_SYMBOLS, LISTING}

    private Type type;
    private String content;

    public PrimeText(Type type, String content) {
        this.type = type;
        this.content = content;
    }

    public Type getType() {
        return type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean addChild(Text let) {
        return false;
    }

    @Override
    public Text getChild(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public String toString() {
        return content;
    }
}
