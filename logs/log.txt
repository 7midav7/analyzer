14/Oct/2015 04:13:42,974 - DEBUG TypeParsingComponent: First, note that you may use ordinary characters as regular expressions�a regular expression
doesn�t have to contain special characters, as you can see in the first call to split( ), which
just splits on whitespace.

14/Oct/2015 04:13:42,993 - DEBUG TypeParsingComponent: The first expression matches the letter f followed by one or more word characters (note that
the w is lowercase this time). It only replaces the first match that it finds, so the word "found"
is replaced by the word "located."

14/Oct/2015 04:13:42,993 - DEBUG TypeParsingComponent: The second expression matches any of the three words separated by the OR vertical bars, and
it replaces all matches that it finds.

14/Oct/2015 04:13:42,994 - DEBUG TypeParsingComponent: You�ll see that the non-String regular expressions have more powerful replacement tools�
for example, you can call methods to perform replacements. Non-String regular expressions
are also significantly more efficient if you need to use the regular expression more than once.

14/Oct/2015 04:13:42,994 - DEBUG TypeParsingComponent: Exercise 9: (4) Using the documentation for java.util.regex.Pattern as a resource,
replace all the vowels in Splitting.knights with underscores.

14/Oct/2015 15:09:43,940 - WARN TextParserThread: Unknown type of com.marchenko.analyzer.parser.TextParserThread$TypeParsingText
14/Oct/2015 15:09:43,942 - WARN TextParserThread: Unknown type of com.marchenko.analyzer.parser.TextParserThread$TypeParsingText
14/Oct/2015 15:09:43,942 - WARN TextParserThread: Unknown type of com.marchenko.analyzer.parser.TextParserThread$TypeParsingText
14/Oct/2015 15:09:43,942 - WARN TextParserThread: Unknown type of com.marchenko.analyzer.parser.TextParserThread$TypeParsingText
14/Oct/2015 15:09:43,942 - WARN TextParserThread: Unknown type of com.marchenko.analyzer.parser.TextParserThread$TypeParsingText
14/Oct/2015 15:09:43,942 - WARN TextParserThread: Unknown type of com.marchenko.analyzer.parser.TextParserThread$TypeParsingText
14/Oct/2015 15:09:43,942 - WARN TextParserThread: Unknown type of com.marchenko.analyzer.parser.TextParserThread$TypeParsingText
14/Oct/2015 15:09:43,943 - WARN TextParserThread: Unknown type of com.marchenko.analyzer.parser.TextParserThread$TypeParsingText
14/Oct/2015 15:09:43,943 - WARN TextParserThread: Unknown type of com.marchenko.analyzer.parser.TextParserThread$TypeParsingText
14/Oct/2015 15:09:43,943 - WARN TextParserThread: Unknown type of com.marchenko.analyzer.parser.TextParserThread$TypeParsingText
14/Oct/2015 15:09:43,943 - WARN TextParserThread: Unknown type of com.marchenko.analyzer.parser.TextParserThread$TypeParsingText
14/Oct/2015 15:09:43,943 - WARN TextParserThread: Unknown type of com.marchenko.analyzer.parser.TextParserThread$TypeParsingText
