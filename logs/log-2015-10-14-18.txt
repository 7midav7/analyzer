14/Oct/2015 03:36:59,874 - DEBUG TypeParsingComponent: Exercise 9: (4) Using the documentation for java.util.regex.Pattern as a resource,
replace all the vowels in Splitting.knights with underscores.
14/Oct/2015 03:37:05,028 - DEBUG TypeParsingComponent: A regular expression is a way to describe strings in general terms, so that you can say, "If a
string has these things in it, then it matches what I�m looking for." For example, to say that a
number might or might not be preceded by a minus sign, you put in the minus sign followed
by a question mark.
14/Oct/2015 03:37:05,031 - DEBUG TypeParsingComponent: To describe an integer, you say that it�s one or more digits. In regular expressions, a digit is
described by saying �\d�. If you have any experience with regular expressions in other
languages, you�ll immediately notice a difference in the way backslashes are handled. In
other languages, �\\� means "I want to insert a plain old (literal) backslash in the regular
expression. Don�t give it any special meaning." In Java, � \ \ � means "I�m inserting a regular
expression backslash, so that the following character has special meaning." For example, if
you want to indicate a digit, your regular expression string will be �\\d�. If you want to insert
a literal backslash, you say �\\\\�- However, things like newlines and tabs just use a single
backslash: �\n\t�.
14/Oct/2015 03:37:05,032 - DEBUG TypeParsingComponent: The simplest way to use regular expressions is to use the functionality built into the String
class. For example, we can see whether a String matches the regular expression above.
14/Oct/2015 03:37:05,032 - DEBUG TypeParsingComponent: //: strings/IntegerMatch.java
public class IntegerMatch {
 public static void main(String[] args) {
 System.out.println("-1234".matches("-?\\d+"));
 System.out.println("5678".matches("-?\\d+"));
 System.out.println("+911".matches("-?\\d+"));
 System.out.println("+911".matches("(-|\\+)?\\d+"));
 }
} /* Output:
true
true
false
true
*///:~
14/Oct/2015 03:37:05,032 - DEBUG TypeParsingComponent: The first two expressions match, but the third one starts with a �+�, which is a legitimate sign
but means the number doesn�t match the regular expression. So we need a way to say, "may
start with a + or a -." In regular expressions, parentheses have the effect of grouping an
expression, and the vertical bar �|� means OR.
14/Oct/2015 03:37:05,032 - DEBUG TypeParsingComponent: A useful regular expression tool that�s built into String is split( ), which means, "Split this
string around matches of the given regular expression."
14/Oct/2015 03:37:05,032 - DEBUG TypeParsingComponent: //: strings/Splitting.java
import java.util.*;
public class Splitting {
 public static String knights =
 "Then, when you have found the shrubbery, you must " +
 "cut down the mightiest tree in the forest... " +
 "with... a herring!";
 public static void split(String regex) {
 System.out.println(
 Arrays.toString(knights.split(regex)));
 }
 public static void main(String[] args) {
 split(" "); // Doesn�t have to contain regex chars
 split("\\W+"); // Non-word characters
 split("n\\W+"); // �n� followed by non-word characters
 }
} /* Output:
[Then,, when, you, have, found, the, shrubbery,, you, must, cut, down,
the, mightiest, tree, in, the, forest..., with..., a, herring!]
[Then, when, you, have, found, the, shrubbery, you, must, cut, down,
the, mightiest, tree, in, the, forest, with, a, herring]
[The, whe, you have found the shrubbery, you must cut dow, the mightiest
tree i, the forest... with... a herring!]
*///:~
14/Oct/2015 03:37:05,033 - DEBUG TypeParsingComponent: First, note that you may use ordinary characters as regular expressions�a regular expression
doesn�t have to contain special characters, as you can see in the first call to split( ), which
just splits on whitespace.
14/Oct/2015 03:37:05,033 - DEBUG TypeParsingComponent: The first expression matches the letter f followed by one or more word characters (note that
the w is lowercase this time). It only replaces the first match that it finds, so the word "found"
is replaced by the word "located."
14/Oct/2015 03:37:05,033 - DEBUG TypeParsingComponent: The second expression matches any of the three words separated by the OR vertical bars, and
it replaces all matches that it finds.
14/Oct/2015 03:37:05,033 - DEBUG TypeParsingComponent: You�ll see that the non-String regular expressions have more powerful replacement tools�
for example, you can call methods to perform replacements. Non-String regular expressions
are also significantly more efficient if you need to use the regular expression more than once.
14/Oct/2015 03:37:05,033 - DEBUG TypeParsingComponent: Exercise 9: (4) Using the documentation for java.util.regex.Pattern as a resource,
replace all the vowels in Splitting.knights with underscores.
14/Oct/2015 03:37:09,836 - DEBUG TypeParsingComponent: A regular expression is a way to describe strings in general terms, so that you can say, "If a
string has these things in it, then it matches what I�m looking for." For example, to say that a
number might or might not be preceded by a minus sign, you put in the minus sign followed
by a question mark.
14/Oct/2015 03:37:09,839 - DEBUG TypeParsingComponent: To describe an integer, you say that it�s one or more digits. In regular expressions, a digit is
described by saying �\d�. If you have any experience with regular expressions in other
languages, you�ll immediately notice a difference in the way backslashes are handled. In
other languages, �\\� means "I want to insert a plain old (literal) backslash in the regular
expression. Don�t give it any special meaning." In Java, � \ \ � means "I�m inserting a regular
expression backslash, so that the following character has special meaning." For example, if
you want to indicate a digit, your regular expression string will be �\\d�. If you want to insert
a literal backslash, you say �\\\\�- However, things like newlines and tabs just use a single
backslash: �\n\t�.
14/Oct/2015 03:37:09,855 - DEBUG TypeParsingComponent: The simplest way to use regular expressions is to use the functionality built into the String
class. For example, we can see whether a String matches the regular expression above.
14/Oct/2015 03:37:09,855 - DEBUG TypeParsingComponent: //: strings/IntegerMatch.java
public class IntegerMatch {
 public static void main(String[] args) {
 System.out.println("-1234".matches("-?\\d+"));
 System.out.println("5678".matches("-?\\d+"));
 System.out.println("+911".matches("-?\\d+"));
 System.out.println("+911".matches("(-|\\+)?\\d+"));
 }
} /* Output:
true
true
false
true
*///:~
14/Oct/2015 03:37:09,856 - DEBUG TypeParsingComponent: The first two expressions match, but the third one starts with a �+�, which is a legitimate sign
but means the number doesn�t match the regular expression. So we need a way to say, "may
start with a + or a -." In regular expressions, parentheses have the effect of grouping an
expression, and the vertical bar �|� means OR.
14/Oct/2015 03:37:09,856 - DEBUG TypeParsingComponent: A useful regular expression tool that�s built into String is split( ), which means, "Split this
string around matches of the given regular expression."
14/Oct/2015 03:37:09,856 - DEBUG TypeParsingComponent: //: strings/Splitting.java
import java.util.*;
public class Splitting {
 public static String knights =
 "Then, when you have found the shrubbery, you must " +
 "cut down the mightiest tree in the forest... " +
 "with... a herring!";
 public static void split(String regex) {
 System.out.println(
 Arrays.toString(knights.split(regex)));
 }
 public static void main(String[] args) {
 split(" "); // Doesn�t have to contain regex chars
 split("\\W+"); // Non-word characters
 split("n\\W+"); // �n� followed by non-word characters
 }
} /* Output:
[Then,, when, you, have, found, the, shrubbery,, you, must, cut, down,
the, mightiest, tree, in, the, forest..., with..., a, herring!]
[Then, when, you, have, found, the, shrubbery, you, must, cut, down,
the, mightiest, tree, in, the, forest, with, a, herring]
[The, whe, you have found the shrubbery, you must cut dow, the mightiest
tree i, the forest... with... a herring!]
*///:~
14/Oct/2015 03:37:09,856 - DEBUG TypeParsingComponent: First, note that you may use ordinary characters as regular expressions�a regular expression
doesn�t have to contain special characters, as you can see in the first call to split( ), which
just splits on whitespace.
14/Oct/2015 03:37:09,856 - DEBUG TypeParsingComponent: The first expression matches the letter f followed by one or more word characters (note that
the w is lowercase this time). It only replaces the first match that it finds, so the word "found"
is replaced by the word "located."
14/Oct/2015 03:37:09,857 - DEBUG TypeParsingComponent: The second expression matches any of the three words separated by the OR vertical bars, and
it replaces all matches that it finds.
14/Oct/2015 03:37:09,857 - DEBUG TypeParsingComponent: You�ll see that the non-String regular expressions have more powerful replacement tools�
for example, you can call methods to perform replacements. Non-String regular expressions
are also significantly more efficient if you need to use the regular expression more than once.
14/Oct/2015 03:37:09,857 - DEBUG TypeParsingComponent: Exercise 9: (4) Using the documentation for java.util.regex.Pattern as a resource,
replace all the vowels in Splitting.knights with underscores.
14/Oct/2015 03:37:14,466 - DEBUG TypeParsingComponent: A regular expression is a way to describe strings in general terms, so that you can say, "If a
string has these things in it, then it matches what I�m looking for." For example, to say that a
number might or might not be preceded by a minus sign, you put in the minus sign followed
by a question mark.
14/Oct/2015 03:37:14,481 - DEBUG TypeParsingComponent: To describe an integer, you say that it�s one or more digits. In regular expressions, a digit is
described by saying �\d�. If you have any experience with regular expressions in other
languages, you�ll immediately notice a difference in the way backslashes are handled. In
other languages, �\\� means "I want to insert a plain old (literal) backslash in the regular
expression. Don�t give it any special meaning." In Java, � \ \ � means "I�m inserting a regular
expression backslash, so that the following character has special meaning." For example, if
you want to indicate a digit, your regular expression string will be �\\d�. If you want to insert
a literal backslash, you say �\\\\�- However, things like newlines and tabs just use a single
backslash: �\n\t�.
14/Oct/2015 03:37:14,482 - DEBUG TypeParsingComponent: The simplest way to use regular expressions is to use the functionality built into the String
class. For example, we can see whether a String matches the regular expression above.
14/Oct/2015 03:37:14,482 - DEBUG TypeParsingComponent: //: strings/IntegerMatch.java
public class IntegerMatch {
 public static void main(String[] args) {
 System.out.println("-1234".matches("-?\\d+"));
 System.out.println("5678".matches("-?\\d+"));
 System.out.println("+911".matches("-?\\d+"));
 System.out.println("+911".matches("(-|\\+)?\\d+"));
 }
} /* Output:
true
true
false
true
*///:~
14/Oct/2015 03:37:14,482 - DEBUG TypeParsingComponent: The first two expressions match, but the third one starts with a �+�, which is a legitimate sign
but means the number doesn�t match the regular expression. So we need a way to say, "may
start with a + or a -." In regular expressions, parentheses have the effect of grouping an
expression, and the vertical bar �|� means OR.
14/Oct/2015 03:37:14,482 - DEBUG TypeParsingComponent: A useful regular expression tool that�s built into String is split( ), which means, "Split this
string around matches of the given regular expression."
14/Oct/2015 03:37:14,482 - DEBUG TypeParsingComponent: //: strings/Splitting.java
import java.util.*;
public class Splitting {
 public static String knights =
 "Then, when you have found the shrubbery, you must " +
 "cut down the mightiest tree in the forest... " +
 "with... a herring!";
 public static void split(String regex) {
 System.out.println(
 Arrays.toString(knights.split(regex)));
 }
 public static void main(String[] args) {
 split(" "); // Doesn�t have to contain regex chars
 split("\\W+"); // Non-word characters
 split("n\\W+"); // �n� followed by non-word characters
 }
} /* Output:
[Then,, when, you, have, found, the, shrubbery,, you, must, cut, down,
the, mightiest, tree, in, the, forest..., with..., a, herring!]
[Then, when, you, have, found, the, shrubbery, you, must, cut, down,
the, mightiest, tree, in, the, forest, with, a, herring]
[The, whe, you have found the shrubbery, you must cut dow, the mightiest
tree i, the forest... with... a herring!]
*///:~
14/Oct/2015 03:37:14,482 - DEBUG TypeParsingComponent: First, note that you may use ordinary characters as regular expressions�a regular expression
doesn�t have to contain special characters, as you can see in the first call to split( ), which
just splits on whitespace.
14/Oct/2015 03:37:14,483 - DEBUG TypeParsingComponent: The first expression matches the letter f followed by one or more word characters (note that
the w is lowercase this time). It only replaces the first match that it finds, so the word "found"
is replaced by the word "located."
14/Oct/2015 03:37:14,483 - DEBUG TypeParsingComponent: The second expression matches any of the three words separated by the OR vertical bars, and
it replaces all matches that it finds.
14/Oct/2015 03:37:14,483 - DEBUG TypeParsingComponent: You�ll see that the non-String regular expressions have more powerful replacement tools�
for example, you can call methods to perform replacements. Non-String regular expressions
are also significantly more efficient if you need to use the regular expression more than once.
14/Oct/2015 03:37:14,483 - DEBUG TypeParsingComponent: Exercise 9: (4) Using the documentation for java.util.regex.Pattern as a resource,
replace all the vowels in Splitting.knights with underscores.
14/Oct/2015 03:37:46,548 - DEBUG TypeParsingComponent: A regular expression is a way to describe strings in general terms, so that you can say, "If a
string has these things in it, then it matches what I�m looking for." For example, to say that a
number might or might not be preceded by a minus sign, you put in the minus sign followed
by a question mark.
14/Oct/2015 03:37:46,565 - DEBUG TypeParsingComponent: To describe an integer, you say that it�s one or more digits. In regular expressions, a digit is
described by saying �\d�. If you have any experience with regular expressions in other
languages, you�ll immediately notice a difference in the way backslashes are handled. In
other languages, �\\� means "I want to insert a plain old (literal) backslash in the regular
expression. Don�t give it any special meaning." In Java, � \ \ � means "I�m inserting a regular
expression backslash, so that the following character has special meaning." For example, if
you want to indicate a digit, your regular expression string will be �\\d�. If you want to insert
a literal backslash, you say �\\\\�- However, things like newlines and tabs just use a single
backslash: �\n\t�.
14/Oct/2015 03:37:46,566 - DEBUG TypeParsingComponent: The simplest way to use regular expressions is to use the functionality built into the String
class. For example, we can see whether a String matches the regular expression above.
14/Oct/2015 03:37:46,566 - DEBUG TypeParsingComponent: //: strings/IntegerMatch.java
public class IntegerMatch {
 public static void main(String[] args) {
 System.out.println("-1234".matches("-?\\d+"));
 System.out.println("5678".matches("-?\\d+"));
 System.out.println("+911".matches("-?\\d+"));
 System.out.println("+911".matches("(-|\\+)?\\d+"));
 }
} /* Output:
true
true
false
true
*///:~
14/Oct/2015 03:37:46,566 - DEBUG TypeParsingComponent: The first two expressions match, but the third one starts with a �+�, which is a legitimate sign
but means the number doesn�t match the regular expression. So we need a way to say, "may
start with a + or a -." In regular expressions, parentheses have the effect of grouping an
expression, and the vertical bar �|� means OR.
14/Oct/2015 03:37:46,566 - DEBUG TypeParsingComponent: A useful regular expression tool that�s built into String is split( ), which means, "Split this
string around matches of the given regular expression."
14/Oct/2015 03:37:46,567 - DEBUG TypeParsingComponent: //: strings/Splitting.java
import java.util.*;
public class Splitting {
 public static String knights =
 "Then, when you have found the shrubbery, you must " +
 "cut down the mightiest tree in the forest... " +
 "with... a herring!";
 public static void split(String regex) {
 System.out.println(
 Arrays.toString(knights.split(regex)));
 }
 public static void main(String[] args) {
 split(" "); // Doesn�t have to contain regex chars
 split("\\W+"); // Non-word characters
 split("n\\W+"); // �n� followed by non-word characters
 }
} /* Output:
[Then,, when, you, have, found, the, shrubbery,, you, must, cut, down,
the, mightiest, tree, in, the, forest..., with..., a, herring!]
[Then, when, you, have, found, the, shrubbery, you, must, cut, down,
the, mightiest, tree, in, the, forest, with, a, herring]
[The, whe, you have found the shrubbery, you must cut dow, the mightiest
tree i, the forest... with... a herring!]
*///:~
14/Oct/2015 03:37:46,567 - DEBUG TypeParsingComponent: First, note that you may use ordinary characters as regular expressions�a regular expression
doesn�t have to contain special characters, as you can see in the first call to split( ), which
just splits on whitespace.
14/Oct/2015 03:37:46,567 - DEBUG TypeParsingComponent: The first expression matches the letter f followed by one or more word characters (note that
the w is lowercase this time). It only replaces the first match that it finds, so the word "found"
is replaced by the word "located."
14/Oct/2015 03:37:46,567 - DEBUG TypeParsingComponent: The second expression matches any of the three words separated by the OR vertical bars, and
it replaces all matches that it finds.
14/Oct/2015 03:37:46,567 - DEBUG TypeParsingComponent: You�ll see that the non-String regular expressions have more powerful replacement tools�
for example, you can call methods to perform replacements. Non-String regular expressions
are also significantly more efficient if you need to use the regular expression more than once.
14/Oct/2015 03:37:46,567 - DEBUG TypeParsingComponent: Exercise 9: (4) Using the documentation for java.util.regex.Pattern as a resource,
replace all the vowels in Splitting.knights with underscores.
14/Oct/2015 03:37:57,809 - DEBUG TypeParsingComponent: A regular expression is a way to describe strings in general terms, so that you can say, "If a
string has these things in it, then it matches what I�m looking for." For example, to say that a
number might or might not be preceded by a minus sign, you put in the minus sign followed
by a question mark.
14/Oct/2015 03:37:57,828 - DEBUG TypeParsingComponent: To describe an integer, you say that it�s one or more digits. In regular expressions, a digit is
described by saying �\d�. If you have any experience with regular expressions in other
languages, you�ll immediately notice a difference in the way backslashes are handled. In
other languages, �\\� means "I want to insert a plain old (literal) backslash in the regular
expression. Don�t give it any special meaning." In Java, � \ \ � means "I�m inserting a regular
expression backslash, so that the following character has special meaning." For example, if
you want to indicate a digit, your regular expression string will be �\\d�. If you want to insert
a literal backslash, you say �\\\\�- However, things like newlines and tabs just use a single
backslash: �\n\t�.
14/Oct/2015 03:37:57,828 - DEBUG TypeParsingComponent: The simplest way to use regular expressions is to use the functionality built into the String
class. For example, we can see whether a String matches the regular expression above.
14/Oct/2015 03:37:57,828 - DEBUG TypeParsingComponent: //: strings/IntegerMatch.java
public class IntegerMatch {
 public static void main(String[] args) {
 System.out.println("-1234".matches("-?\\d+"));
 System.out.println("5678".matches("-?\\d+"));
 System.out.println("+911".matches("-?\\d+"));
 System.out.println("+911".matches("(-|\\+)?\\d+"));
 }
} /* Output:
true
true
false
true
*///:~
14/Oct/2015 03:37:57,828 - DEBUG TypeParsingComponent: The first two expressions match, but the third one starts with a �+�, which is a legitimate sign
but means the number doesn�t match the regular expression. So we need a way to say, "may
start with a + or a -." In regular expressions, parentheses have the effect of grouping an
expression, and the vertical bar �|� means OR.
14/Oct/2015 03:37:57,828 - DEBUG TypeParsingComponent: A useful regular expression tool that�s built into String is split( ), which means, "Split this
string around matches of the given regular expression."
14/Oct/2015 03:37:57,829 - DEBUG TypeParsingComponent: //: strings/Splitting.java
import java.util.*;
public class Splitting {
 public static String knights =
 "Then, when you have found the shrubbery, you must " +
 "cut down the mightiest tree in the forest... " +
 "with... a herring!";
 public static void split(String regex) {
 System.out.println(
 Arrays.toString(knights.split(regex)));
 }
 public static void main(String[] args) {
 split(" "); // Doesn�t have to contain regex chars
 split("\\W+"); // Non-word characters
 split("n\\W+"); // �n� followed by non-word characters
 }
} /* Output:
[Then,, when, you, have, found, the, shrubbery,, you, must, cut, down,
the, mightiest, tree, in, the, forest..., with..., a, herring!]
[Then, when, you, have, found, the, shrubbery, you, must, cut, down,
the, mightiest, tree, in, the, forest, with, a, herring]
[The, whe, you have found the shrubbery, you must cut dow, the mightiest
tree i, the forest... with... a herring!]
*///:~
14/Oct/2015 03:37:57,829 - DEBUG TypeParsingComponent: First, note that you may use ordinary characters as regular expressions�a regular expression
doesn�t have to contain special characters, as you can see in the first call to split( ), which
just splits on whitespace.
14/Oct/2015 03:37:57,829 - DEBUG TypeParsingComponent: The first expression matches the letter f followed by one or more word characters (note that
the w is lowercase this time). It only replaces the first match that it finds, so the word "found"
is replaced by the word "located."
14/Oct/2015 03:37:57,829 - DEBUG TypeParsingComponent: The second expression matches any of the three words separated by the OR vertical bars, and
it replaces all matches that it finds.
14/Oct/2015 03:37:57,829 - DEBUG TypeParsingComponent: You�ll see that the non-String regular expressions have more powerful replacement tools�
for example, you can call methods to perform replacements. Non-String regular expressions
are also significantly more efficient if you need to use the regular expression more than once.
14/Oct/2015 03:37:57,830 - DEBUG TypeParsingComponent: Exercise 9: (4) Using the documentation for java.util.regex.Pattern as a resource,
replace all the vowels in Splitting.knights with underscores.
